<?php

namespace D2D\SearchEngineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

    public function indexAction() {
        return $this->render('D2DSearchEngineBundle:Default:index.html.twig');
    }

    public function SearchFormAction() {
        return $this->render('D2DSearchEngineBundle:Default:SearchForm.html.twig');
    }

    public function SearchResultAction(Request $request) {
        $url = $this->getURlChanged($request);
        $objData = $this->getObjDataFromURl($url);
        return $this->render('D2DSearchEngineBundle:Default:SearchResult.html.twig', array(
                    "arrayData" => $objData,
        ));
    }

    public function getURlChanged(Request $request) {
        $paramsSqlPath = $request->get('sqlPath');
        $paramsNumPo = $request->get('numPo');
        $paramsSkuId = $request->get('SkuId');
        $urlChanged = 'https://test-sy.fcsystem.com/web/app_dev.php/GenericWsGetJsonFromSqlBundle/getSqlToJson/'
                . $paramsSqlPath 
                . '/?numPo=' . $paramsNumPo 
                . '&skuId=' . $paramsSkuId 
                . '&importer=&pol=&pod=&dpt=';
        return $urlChanged;
    }

    public function getObjDataFromURl($url) {
        $jsonFile = file_get_contents($url);
        $obj = json_decode($jsonFile);
        $objData = $this->getArrayData($obj);
        return $objData;
    }

    public function getArrayData($jsonObj) {
        $dataOfJsonObj = $jsonObj->{'data'};
        $newArrayData = array();
        foreach ($dataOfJsonObj as $id => $row) {
            $newRow = $this->getArrayDataRow($row);
            array_push($newArrayData, $newRow);
        }
        return $newArrayData;
    }

    public function getArrayDataRow($row) {
        $newRow = array();
        foreach ($row as $key => $value) {
            $newRow[$key] = $value;
        }
        return $newRow;
    }

}
