function showDataForNumPo() {
    url = getURL();
    var params = new URLSearchParams();
    //faire un cas ou il n'y a pas de numPo ! 
    params.set("numPo", getValueFromInput("numPo"));
    params.set("skuId", getValueFromInput("skuId"));
    params.set("sqlPath", getValueFromInput("sqlPath"));
    url.search = params.toString();
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'html',
        success: function (result) {
            $("#divSearchResult").html(result);
            console.log(url);
        }});
}

function getValueFromInput(whichInput) {
    var input = document.getElementById(whichInput);
    return input.value;
}

function getURL() {
    var url = new URL(document.location);
    var newUrl = "http://" + url.hostname + url.pathname;
    var url = new URL(newUrl.replace(/SearchForm/g, "SearchResult"));
    return url;
}

